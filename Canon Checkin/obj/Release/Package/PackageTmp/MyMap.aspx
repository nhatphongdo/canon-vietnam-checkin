﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="MyMap.aspx.cs" Inherits="Canon_Checkin.MyMap" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <asp:Literal runat="server" ID="MapInfo"></asp:Literal>
    <link href="Content/jquery.jscrollpane.css" rel="stylesheet" />
    <script src="Scripts/jquery.cj_object_scaler.js"></script>
    <script src="Scripts/mwheelIntent.js"></script>
    <script src="Scripts/jquery.mousewheel.js"></script>
    <script src="Scripts/jquery.jscrollpane.min.js"></script>
    <script type="text/javascript">
        var currentImage = 0;
        $(document).ready(function () {
            setMenu(2);
            initializeMaps();
            setPage('gallery');

            $('.lucky-list div').jScrollPane();

            $('.album ul').width($('.album ul li').length * 600);

            $('.left').on("click", function () {
                ++currentImage;
                if (currentImage >= $('.album ul li').length) {
                    currentImage = $('.album ul li').length - 1;
                }

                $('.album ul').animate({
                    "margin-left": currentImage * -600
                }, 500);

                $('.message').html("");
            });
            $('.right').on("click", function () {
                --currentImage;
                if (currentImage < 0) {
                    currentImage = 0;
                }

                $('.album ul').animate({
                    "margin-left": currentImage * -600
                }, 500);

                $('.message').html("");
            });

            $('#album-popup .close').on('click', function () {
                $('#album-popup').fadeOut();
            });

            var canvas = document.getElementById("map-canvas");
            canvas.onclick = function (e) {
                var selection;
                for (var idx = 0; idx < mapNames.length; idx++) {
                    var image = preload.getResult(mapNames[idx]);
                    var map = getMap(mapNames[idx]);
                    if (!isTransparentUnderMouse(canvas, image.result, map, e)) {
                        selection = mapNames[idx];
                    }
                }

                if (selection != undefined) {
                    window.location.href = "MyMap.aspx?region=" + selection;
                }
            };
        });
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <canvas id="map-canvas" class="map" width="670" height="950"></canvas>
    <div class="head-text">
        Những nơi <strong>
            <asp:Literal runat="server" ID="Name"></asp:Literal></strong> đã đi qua!
    </div>
    <div class="box lucky-list">
        Những mã số may mắn của bạn:
        <br />
        <br />
        <div style="width: 100%; height: 170px; overflow: hidden;">
            <p style="margin: 0px;">
                <asp:Literal runat="server" ID="LuckyCodes"></asp:Literal>
            </p>
        </div>
    </div>
    <div id="album-popup" class="shadow">
        <div class="box">
            <a class="close">X</a>
            <div class="clear"></div>
            <a class="left"></a>
            <div class="album">
                <ul>
                    <asp:Repeater runat="server" ID="Images">
                        <ItemTemplate>
                            <li>
                                <div class="image-box">
                                    <img src="<%# Eval("PhotoPath") %>" alt="" />
                                </div>
                                <div style="margin-top: 10px; line-height: 27px;">
                                    <strong id="like_<%# Eval("ID") %>"><%# Eval("Likes") %></strong> người thích
                                    <a class="share-button float-right" onclick="shareOnFacebook(<%# Eval("ID") %>, '<%# Eval("PhotoPath") %>')"></a>
                                    <a class="like-button float-right" onclick="like(<%# Eval("ID") %>);"></a>
                                    <span id='message_<%# Eval("ID") %>' class="message float-right"></span>
                                </div>
                                <div class="clear" style="text-align: right;">
                                </div>
                                <div style="margin-top: 20px; color: #919191; line-height: 20px;">
                                    <strong style="font-size: 12px; color: #fff;"><%# Eval("Fullname") %></strong>
                                    <br />
                                    Chụp tại: <strong><%# Eval("Place") %>, <%# Eval("City1") %></strong>
                                </div>
                            </li>
                        </ItemTemplate>
                    </asp:Repeater>
                </ul>
            </div>
            <a class="right"></a>
            <div class="clear"></div>
        </div>
    </div>
</asp:Content>
