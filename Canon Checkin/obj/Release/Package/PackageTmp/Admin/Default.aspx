﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Canon_Checkin.Admin.Default" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v12.1, Version=12.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v12.1, Version=12.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.1, Version=12.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Data.Linq" TagPrefix="cc1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>CANON-CHECKIN</title>
    <link href="http://fonts.googleapis.com/css?family=Oswald" rel="stylesheet" type="text/css" />
    <link href="../Content/style.css" rel="stylesheet" type="text/css" media="screen" />
</head>
<body>
    <form id="form1" runat="server">
        <div id="wrapper">
            <div id="header">
                <div id="logo">
                    <h5><a href="#">Canon Checkin</a></h5>
                </div>
            </div>
            <asp:MultiView runat="server" ID="AdminViews">
                <asp:View runat="server" ID="LoginView">
                    <asp:Login ID="Login1" runat="server" Font-Names="Arial" Style="margin: auto;" OnAuthenticate="Login1_Authenticate">
                    </asp:Login>
                </asp:View>
                <asp:View runat="server" ID="MainView">
                    <asp:Panel ID="RadPane1" runat="server" BorderStyle="None" Font-Names="Arial" GroupingText="Thống kê">
                        Có
                        <strong>
                            <asp:Literal runat="server" ID="TotalUsers">0</asp:Literal></strong>
                        người tham gia với 
                       <strong>
                           <asp:Literal runat="server" ID="TotalPhotos">0</asp:Literal></strong>
                        ảnh
                       <br />
                        <label class="float-left">Filter by "Upload date"</label>
                        <label class="float-left">&nbsp;&nbsp;from&nbsp;&nbsp;</label>
                        <div class="float-left">
                            <dx:ASPxDateEdit ID="FromDate" runat="server"></dx:ASPxDateEdit>
                        </div>
                        <label class="float-left">&nbsp;&nbsp;to&nbsp;&nbsp;</label>
                        <div class="float-left">
                            <dx:ASPxDateEdit ID="ToDate" runat="server"></dx:ASPxDateEdit>
                        </div>
                        <div class="float-left" style="margin-left: 10px;">
                            <dx:ASPxButton ID="FilterButton" runat="server" Text="Filter" OnClick="FilterButton_Click"></dx:ASPxButton>
                        </div>
                        <div class="float-left" style="margin-left: 10px;">
                            <dx:ASPxButton ID="ClearFilter" runat="server" Text="Clear Filter" OnClick="ClearFilter_Click"></dx:ASPxButton>
                        </div>
                    </asp:Panel>
                    <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" DataSourceID="LinqDataSource1" EnableTheming="True" Theme="Default" OnCustomButtonCallback="ASPxGridView1_CustomButtonCallback">
                        <Columns>
                            <dx:GridViewDataTextColumn Caption="Full Name" FieldName="User.FullName" VisibleIndex="1">
                                <Settings AllowAutoFilter="True" AutoFilterCondition="Contains" />
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataImageColumn FieldName="PhotoPath" VisibleIndex="2">
                                <PropertiesImage ImageUrlFormatString="~/{0}" ImageWidth="300px">
                                    <Style HorizontalAlign="Center" VerticalAlign="Middle">
                                   </Style>
                                </PropertiesImage>
                            </dx:GridViewDataImageColumn>
                            <dx:GridViewDataDateColumn FieldName="UploadedDate" VisibleIndex="3">
                            </dx:GridViewDataDateColumn>
                            <dx:GridViewDataTextColumn FieldName="Likes" VisibleIndex="4">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="IP" VisibleIndex="5">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataCheckColumn FieldName="IsPublished" VisibleIndex="10">
                            </dx:GridViewDataCheckColumn>
                            <dx:GridViewDataTextColumn FieldName="City1.City1" VisibleIndex="6" Caption="City">
                                <Settings AutoFilterCondition="Contains" />
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Place" VisibleIndex="7">
                                <Settings AutoFilterCondition="Contains" />
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="LuckyCode" VisibleIndex="8">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewCommandColumn ButtonType="Button" Caption="Published" ShowSelectCheckbox="False" VisibleIndex="12">
                                <CustomButtons>
                                    <dx:GridViewCommandColumnCustomButton ID="Publish" Text="Publish/UnPublish">
                                        <Image ToolTip="Publish/UnPulish" Url="~/Images/Publish.png"></Image>
                                    </dx:GridViewCommandColumnCustomButton>
                                </CustomButtons>
                            </dx:GridViewCommandColumn>
                            <dx:GridViewDataTextColumn Caption="ID" FieldName="ID" Visible="false" VisibleIndex="0">
                            </dx:GridViewDataTextColumn>
                        </Columns>
                        <Settings ShowFilterRow="True" ShowGroupPanel="True" />
                        <Styles>
                            <Cell HorizontalAlign="Center" VerticalAlign="Middle">
                            </Cell>
                        </Styles>
                    </dx:ASPxGridView>
                    <asp:LinqDataSource ID="LinqDataSource1" runat="server" ContextTypeName="Canon_Checkin.Canon_CheckinEntities" EntityTypeName="" TableName="Photos" Where="UploadedDate &gt;= @UploadedDate &amp;&amp; UploadedDate &lt;= @UploadedDate1">
                        <WhereParameters>
                            <asp:ControlParameter ControlID="FromDate" DefaultValue="2012-12-10" Name="UploadedDate" PropertyName="Value" Type="DateTime" />
                            <asp:ControlParameter ControlID="ToDate" DefaultValue="2013-12-10" Name="UploadedDate1" PropertyName="Value" Type="DateTime" />
                        </WhereParameters>
                    </asp:LinqDataSource>
                </asp:View>
            </asp:MultiView>
        </div>
        <div id="footer">
            <p>Copyright (c) 2012. All rights reserved. Design by <a href="http://vietdev.vn/">VietDev</a>.</p>
        </div>
    </form>
</body>
</html>
