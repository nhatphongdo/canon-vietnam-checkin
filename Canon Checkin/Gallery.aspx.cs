﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;

namespace Canon_Checkin
{
    public partial class Gallery : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                using (var entities = new Canon_CheckinEntities())
                {
                    var cities = entities.Cities.OrderBy(c => c.City1).ToList().Select(c => new
                                                                                                {
                                                                                                    ID =
                                                                                                c.ID.ToString() + "_" +
                                                                                                c.Region,
                                                                                                    City = c.City1
                                                                                                }).ToList();

                    cities.Insert(0, new
                                         {
                                             ID = "",
                                             City = "Chọn tỉnh / thành"
                                         });

                    City.DataSource = cities;
                    City.DataBind();

                    SelectedCity.Text = string.Empty;

                    if (!string.IsNullOrEmpty(Request["city"]))
                    {
                        City.SelectedValue = Request["city"];
                    }

                    if (!string.IsNullOrEmpty(Request["order"]))
                    {
                        OrderBy.SelectedValue = Request["order"];
                    }

                    if (!string.IsNullOrEmpty(Request["id"]))
                    {
                        var id = 0;
                        if (int.TryParse(Request["id"], out id))
                        {
                            var photo = entities.Photos.FirstOrDefault(p => p.ID == id);
                            if (photo != null)
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "showImageFunc",
                                                                    string.Format(
                                                                        "<script type='text/javascript'>showImage({0}, '{1}', {2}, '{3}', '{4}', '{5}');</script>",
                                                                        photo.ID, photo.PhotoPath, photo.Likes,
                                                                        photo.User.FullName, photo.Place,
                                                                        photo.City1.City1),
                                                                    false);
                            }
                        }
                    }
                    else if (!string.IsNullOrEmpty(Request["page"]) || !string.IsNullOrEmpty(Request["city"]))
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "showGalleryFunc",
                                                            "<script type='text/javascript'>showGallery();</script>",
                                                            false);
                    }
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "showGalleryFunc", "<script type='text/javascript'>showGallery();</script>", false);
            }

            using (var entities = new Canon_CheckinEntities())
            {
                var photos = entities.Photos.Where(p => p.IsPublished);
                
                RenderImageIntoRegion();

                // Set JSON object for draw images on map
                //var jsonPhotos = photos.Select(p => new
                //{
                //    p.PhotoPath,
                //    p.City1.Region,
                //    p.Place
                //});

                // add image to json
                var jsonObject = "";
                jsonObject = "<script type='text/javascript'>";
                jsonObject += "var mapInfo = true;";
                //jsonObject += "var mapInfo = [";
                //foreach (var photo in jsonPhotos)
                //{
                //    var fileInfo = new System.IO.FileInfo(Server.MapPath(photo.PhotoPath));
                //    var thumbPhoto = "Upload/Thumb/" + fileInfo.Name.Replace(fileInfo.Extension, "_thumb" + fileInfo.Extension);

                //    if (!System.IO.File.Exists(Server.MapPath(thumbPhoto)))
                //    {
                //        if (fileInfo.Exists)
                //        {
                //            var bitmap = new Bitmap(System.Drawing.Image.FromFile(Server.MapPath(photo.PhotoPath)), 10, 10);
                //            bitmap.Save(Server.MapPath(thumbPhoto), System.Drawing.Imaging.ImageFormat.Jpeg);
                //        }
                //    }

                //    jsonObject += string.Format("{{src:'{0}', place:'{1}', region:'{2}'}},", thumbPhoto, photo.Place, photo.Region);
                //}
                //jsonObject = jsonObject.TrimEnd(',') + "];";
                jsonObject += "</script>";
                MapInfo.Text = jsonObject;


                // Filter by City
                SelectedCity.Text = string.Empty;
                var cityID = 0;
                if (!string.IsNullOrEmpty(City.SelectedValue))
                {
                    var parts = City.SelectedValue.Split(new char[]
                                                             {
                                                                 '_'
                                                             }, StringSplitOptions.RemoveEmptyEntries);

                    if (parts.Length > 0 && int.TryParse(parts[0], out cityID) && cityID > 0)
                    {
                        photos = photos.Where(p => p.City == cityID);
                    }
                    SelectedCity.Text = City.SelectedItem.Text;
                }
                else if (!string.IsNullOrEmpty(Request["city"]))
                {
                    var parts = Request["city"].Split(new char[]
                                                          {
                                                              '_'
                                                          }, StringSplitOptions.RemoveEmptyEntries);

                    if (parts.Length > 1 && int.TryParse(parts[0], out cityID))
                    {
                        if (cityID > 0)
                        {
                            photos = photos.Where(p => p.City == cityID);
                        }
                        else
                        {
                            var region = parts[1].ToLower();
                            var citiesInRegion = entities.Cities.Where(c => c.Region.ToLower() == region).Select(c => c.ID);
                            photos = photos.Where(p => citiesInRegion.Contains(p.City));
                        }
                    }
                }

                // Order
                if (OrderBy.SelectedValue == "vote")
                {
                    photos = photos.OrderByDescending(p => p.Likes);
                }
                else
                {
                    photos = photos.OrderByDescending(p => p.UploadedDate);
                }

                // Paging properties
                var pageSize = 16;
                var totalItems = photos.Count();
                var pageCount = totalItems / pageSize;
                if (totalItems % pageSize != 0)
                {
                    ++pageCount;
                }

                // Paging
                var page = 1;
                if (!int.TryParse(Request["page"], out page))
                {
                    page = 1;
                }
                photos = photos.Skip((page - 1) * pageSize).Take(pageSize);

                var result = photos.Select(p => new
                                                    {
                                                        p.ID,
                                                        p.PhotoPath,
                                                        p.Place,
                                                        p.Likes,
                                                        Fullname = p.User.FullName,
                                                        p.City1.City1
                                                    }).ToList();

                ImagesList.DataSource = result;
                ImagesList.DataBind();

                var pages = new int[pageCount];
                for (var i = 1; i <= pageCount; i++)
                {
                    pages[i - 1] = i;
                }
                PagingItems.DataSource = pages;
                PagingItems.DataBind();
            }
            
        }

        protected void RenderImageIntoRegion()
        {
            List<ListItem> listitem = new List<ListItem>();
            listitem.Add(new ListItem("Images/north.png", "north"));
            listitem.Add(new ListItem("Images/northern-middle.png", "northern-middle"));
            listitem.Add(new ListItem("Images/pink-river-land.png", "pink-river-land"));
            listitem.Add(new ListItem("Images/southern-middle.png", "southern-middle"));
            listitem.Add(new ListItem("Images/western-highlands.png", "western-highlands"));
            listitem.Add(new ListItem("Images/eastern-south.png", "eastern-south"));
            listitem.Add(new ListItem("Images/western-south.png", "western-south"));
            listitem.Add(new ListItem("Images/island.png", "island"));

            DateTime date = DateTime.Today;
            
            foreach (var item in listitem)
            {
                string nameRegion = item.Value + date.Day+date.Month+date.Year+ ".png";
                if (!System.IO.File.Exists(Server.MapPath("Images/" + nameRegion)))
                {
                    RenderMap(item.Text, item.Value);
                }
            }
        }

        private void RenderMap(string imageUrl, string regionName)
        {
            int thumbsize = 10;
            //load image data
            var entities = new Canon_CheckinEntities();
            var imageData = entities.Photos.Where(p => p.City1.Region == regionName && p.IsPublished == true).ToList();
            int countImage = 0;
            using (var imageRegion = new Bitmap(Server.MapPath(imageUrl)))
            {
                Bitmap drawRegion = new Bitmap(imageRegion.Width, imageRegion.Height);
                using (Graphics graph = Graphics.FromImage(drawRegion))
                {
                    for (int y = 0; y < imageRegion.Height - 10; y += thumbsize + 1)
                    {
                        for (int x = 0; x < imageRegion.Width - 10; x += thumbsize + 1)
                        {
                            if (countImage < imageData.Count() && imageRegion.GetPixel(x, y).A > 70 && imageRegion.GetPixel(x + thumbsize, y + thumbsize).A > 70)
                            {
                                try
                                {
                                    System.Drawing.Image drawImage = System.Drawing.Image.FromFile(Server.MapPath(imageData[countImage].PhotoPath));
                                    graph.DrawImage(drawImage, x, y, thumbsize, thumbsize);
                                    countImage++;
                                }
                                catch (Exception ex)
                                {
                                    continue;
                                }
                            }

                        }
                    }
                    DateTime date = DateTime.Today;
                    drawRegion.Save(Server.MapPath("Images/" + regionName + date.Day+date.Month+date.Year + ".png"), System.Drawing.Imaging.ImageFormat.Png);

                }

            }


        }
    }
}