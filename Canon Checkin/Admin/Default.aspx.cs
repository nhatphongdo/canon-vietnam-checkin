﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using DevExpress.Web.ASPxGridView;
using System.Collections;

namespace Canon_Checkin.Admin
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            AdminViews.SetActiveView(LoginView);
            var cookie = Request.Cookies[FormsAuthentication.FormsCookieName];
            if (cookie != null && !string.IsNullOrEmpty(cookie.Value))
            {
                var formTicket = FormsAuthentication.Decrypt(cookie.Value);
                if (!formTicket.Expired && formTicket.Name.ToLower() == "admin")
                {
                    AdminViews.SetActiveView(MainView);
                    var entities = new Canon_CheckinEntities();
                    TotalUsers.Text = entities.Users.Count().ToString();
                    TotalPhotos.Text = entities.Photos.Count().ToString();
                }
            }

        }

        protected void Login1_Authenticate(object sender, AuthenticateEventArgs e)
        {
            if (Login1.UserName.ToLower() == "admin" && Login1.Password == "canonchekin123#@!")
            {
                FormsAuthentication.SetAuthCookie("Admin", true);
                Response.Redirect("Default.aspx");
            }
        }

        protected void ASPxGridView1_CustomButtonCallback(object sender, ASPxGridViewCustomButtonCallbackEventArgs e)
        {
            if (e.ButtonID == "Publish")
            {
                int rowid = Convert.ToInt32(ASPxGridView1.GetRowValues(e.VisibleIndex, "ID"));
                if (rowid > 0)
                {
                    using (var entities = new Canon_CheckinEntities())
                    {
                        var data = entities.Photos.Where(p => p.ID == rowid).SingleOrDefault();
                        data.IsPublished = !data.IsPublished;
                        entities.SaveChanges();
                        ASPxGridView1.DataBind();
                    }
                }
                
            }
            
        }

        protected void FilterButton_Click(object sender, EventArgs e)
        {
            ASPxGridView1.DataBind();
        }

        protected void ClearFilter_Click(object sender, EventArgs e)
        {
            ToDate.Value = null;
            FromDate.Value = null;
            ASPxGridView1.DataBind();
        }


    }
}