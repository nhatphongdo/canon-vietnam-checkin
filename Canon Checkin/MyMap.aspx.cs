﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Canon_Checkin
{
    public partial class MyMap : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["FacebookID"] == null)
            {
                Response.Redirect("Default.aspx");
                return;
            }

            using (var entities = new Canon_CheckinEntities())
            {
                var facebookID = Session["FacebookID"].ToString();
                var user = entities.Users.FirstOrDefault(u => u.FacebookID == facebookID);
                if (user == null)
                {
                    Response.Redirect("Default.aspx");
                    return;
                }

                Name.Text = user.FullName;

                IQueryable<int> citiesInRegion;
                if (string.IsNullOrEmpty(Request["region"]))
                {
                    citiesInRegion = entities.Cities.Select(c => c.ID);
                }
                else
                {
                    var region = Request["region"].ToLower();
                    citiesInRegion = entities.Cities.Where(c => c.Region.ToLower() == region).Select(c => c.ID);

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "showAlbumFunc", "<script type='text/javascript'>showAlbum();</script>", false);
                }

                // Set JSON object for draw images on map
                var jsonPhotos = entities.Photos.Where(p => p.UserID == user.ID && p.IsPublished).Select(p => new
                {
                    p.PhotoPath,
                    p.City1.Region,
                    p.Place,
                    p.LuckyCode
                });
                var jsonObject = "";

                jsonObject = "<script type='text/javascript'>";
                jsonObject += "var mapInfo = [";
                foreach (var photo in jsonPhotos)
                {
                    var fileInfo = new System.IO.FileInfo(Server.MapPath(photo.PhotoPath));
                    var thumbPhoto = "Upload/Thumb/" + fileInfo.Name.Replace(fileInfo.Extension, "_thumb" + fileInfo.Extension);

                    if (!System.IO.File.Exists(Server.MapPath(thumbPhoto)))
                    {
                        if (fileInfo.Exists)
                        {
                            var bitmap = new Bitmap(System.Drawing.Image.FromFile(Server.MapPath(photo.PhotoPath)), 10, 10);
                            bitmap.Save(Server.MapPath(thumbPhoto), System.Drawing.Imaging.ImageFormat.Jpeg);
                        }
                    }

                    jsonObject += string.Format("{{src:'{0}', place:'{1}', region:'{2}'}},", thumbPhoto, photo.Place, photo.Region);

                    LuckyCodes.Text += photo.LuckyCode + "<br />";
                }
                jsonObject = jsonObject.TrimEnd(',') + "];";
                jsonObject += "</script>";
                MapInfo.Text = jsonObject;

                var photos = entities.Photos.Where(p => p.UserID == user.ID && p.IsPublished && citiesInRegion.Contains(p.City))
                    .Select(p => new
                                   {
                                       Fullname = user.FullName,
                                       p.ID,
                                       p.PhotoPath,
                                       p.Likes,
                                       p.LuckyCode,
                                       p.Place,
                                       p.City1.Region,
                                       p.City1.City1
                                   }).ToList();

                Images.DataSource = photos;
                Images.DataBind();
            }
        }
    }
}