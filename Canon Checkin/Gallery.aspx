﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Gallery.aspx.cs" Inherits="Canon_Checkin.Gallery" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <asp:Literal runat="server" ID="MapInfo"></asp:Literal>
    <asp:Literal runat="server" ID="MapInfo1"></asp:Literal>
    <asp:Literal runat="server" ID="MapInfo2"></asp:Literal>
    <asp:Literal runat="server" ID="MapInfo3"></asp:Literal>
    <asp:Literal runat="server" ID="MapInfo4"></asp:Literal>
    <script src="Scripts/jquery.cj_object_scaler.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            setMenu(3);
            initializeMaps();
            setPage('gallery');

            var id = $('#City').val();
            var parts = id.split('_', 2);
            
            var titleIdx = mapNames.indexOf(parts[1]);
            $('.box-header').text(mapTitles[titleIdx]);
            
            $('#gallery-popup .close').click(function () {
                $('#gallery-popup').fadeOut();
            });

            $('#album-popup .close').click(function () {
                $('#album-popup').fadeOut();
            });

            var canvas = document.getElementById("map-canvas");
            canvas.onclick = function (e) {
                var selection;
                for (var idx = 0; idx < mapNames.length; idx++) {
                    var image = preload.getResult(mapNames[idx]);
                    var map = getMap(mapNames[idx]);
                    if (!isTransparentUnderMouse(canvas, image.result, map, e)) {
                        selection = mapNames[idx];
                    }
                }

                if (selection != undefined) {
                    window.location.href = "Gallery.aspx?city=0_" + selection;
                }
            };
        })
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <canvas id="map-canvas" class="map" width="670" height="950"></canvas>
    <div id="gallery-popup" class="shadow">
        <div class="box">
            <a class="close">X</a>
            <div class="box-header"></div>
            <div class="search-box">
                <span class="title">
                    <asp:Literal runat="server" ID="SelectedCity"></asp:Literal></span>
                <asp:DropDownList runat="server" ID="City" ClientIDMode="Static" CssClass="combo-box float-right" Width="200" DataTextField="City" DataValueField="ID" AutoPostBack="True" />
                <asp:DropDownList runat="server" ID="OrderBy" ClientIDMode="Static" CssClass="combo-box float-right" Width="120" AutoPostBack="True" Style="margin-right: 10px;">
                    <Items>
                        <asp:ListItem Text="Thời gian" Value="time"></asp:ListItem>
                        <asp:ListItem Text="Bình chọn" Value="vote"></asp:ListItem>
                    </Items>
                </asp:DropDownList>
                <label class="float-right" style="margin-right: 5px; color: #fff; line-height: 35px;">Tìm kiếm:</label>
                <div class="clear"></div>
            </div>
            <div class="images-box">
                <ul>
                    <asp:Repeater runat="server" ID="ImagesList">
                        <ItemTemplate>
                            <li>
                                <div class="image">
                                    <a href="javascript:void(0);" onclick="showImage(<%# Eval("ID") %>, '<%# Eval("PhotoPath") %>', <%# Eval("Likes") %>, '<%# Eval("Fullname") %>', '<%# Eval("Place") %>', '<%# Eval("City1") %>');">
                                        <img src="<%# Eval("PhotoPath") %>" alt="" />
                                    </a>
                                </div>
                                <div class="like">
                                    <span><%# Eval("Likes") %></span> người thích
                                </div>
                                <div class="name">
                                    <%# Eval("Fullname") %>
                                </div>
                                <div class="place">
                                    Chụp tại: <span><%# Eval("Place") %></span>
                                </div>
                            </li>
                        </ItemTemplate>
                    </asp:Repeater>
                </ul>
                <div class="clear"></div>
            </div>
            <div class="paging-box">
                <ul>
                    <asp:Repeater runat="server" ID="PagingItems">
                        <ItemTemplate>
                            <li>
                                <a class="<%# (Request["page"] == Container.DataItem.ToString() || (string.IsNullOrEmpty(Request["page"]) && (int)Container.DataItem == 1)) ? "selected" : "" %>"
                                    href="Gallery.aspx?page=<%# Container.DataItem %>&city=<%= string.IsNullOrEmpty(City.SelectedValue) ? Request["city"] : City.SelectedValue %>&order=<%= OrderBy.SelectedValue %>">
                                    <%# Container.DataItem %>
                                </a>
                            </li>
                        </ItemTemplate>
                    </asp:Repeater>
                </ul>
                <div class="clear"></div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
    <div id="album-popup" class="shadow">
        <div class="box" style="width: 600px;">
            <a class="close">X</a>
            <div class="clear"></div>
            <div class="album">
                <ul>
                    <li>
                        <div class="image-box">
                            <img alt="" />
                        </div>
                        <div style="margin-top: 10px; line-height: 27px;">
                            <strong id='like_' class="like-number"></strong>&nbsp;người thích
                            <a class="share-button float-right"></a>
                            <a class="like-button float-right"></a>
                            <span id='message_' class="message float-right"></span>
                        </div>
                        <div class="clear" style="text-align: right;">
                        </div>
                        <div style="margin-top: 20px; color: #919191; line-height: 20px;">
                            <strong id="fullname" style="font-size: 12px; color: #fff;"></strong>
                            <br />
                            Chụp tại: <strong id="place"></strong>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</asp:Content>
