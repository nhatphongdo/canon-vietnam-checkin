﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Term.aspx.cs" Inherits="Canon_Checkin.Term" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link href="Content/jquery.jscrollpane.css" rel="stylesheet" />
    <script src="Scripts/mwheelIntent.js"></script>
    <script src="Scripts/jquery.mousewheel.js"></script>
    <script src="Scripts/jquery.jscrollpane.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            setMenu(4);
            setPage('term');
            $('.loading').fadeOut();

            $('.content').jScrollPane();
        })
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="content">
        <h2>CUỘC THI ẢNH “CHECK IN WITH IXUS”</h2>
        <h2>THỂ LỆ THAM DỰ</h2>
        <br />
        <br />
        <strong>I- Đối tượng dự thi:</strong>
        <ul>
            <li>Tất cả những bạn yêu thích chụp ảnh và du lịch.</li>
            <li>Công dân Việt Nam không giới hạn độ tuổi và vùng miền.</li>
            <li>Thời gian: từ ngày <strong>03/01/2013</strong> đến hết ngày <strong>23/01/2013</strong></li>
        </ul>
        <strong>II- Thể lệ tham dự:</strong>
        <ul>
            <li>Người tham dự “Like” fanpage Canon IXUS Vietnam và truy cập vào ứng dụng “Check-in with IXUS” để tham gia.</li>
            <li>Trong thời gian cuộc thi diễn ra, người tham dự đăng ảnh của mình lên trên ứng dụng, chọn tỉnh/thành nơi đã chụp bức ảnh và ghi vào địa danh cụ thể.</li>
            <li>Mỗi người chơi được quyền đăng nhiều ảnh tham dự. Ảnh sẽ được tự động cập nhật trên “Bản đồ IXUS” và “Bản đồ” của người chơi.</li>
            <li>Sau mỗi lần đăng 1 bức ảnh, người chơi sẽ nhận được mã số may mắn. Cuối chương trình sẽ có phần “Rút thăm may mắn”. 2 người chơi may mắn sẽ nhận “Giải may mắn” của chương trình. </li>
            <li>Người chơi kêu gọi bạn bè bình chọn cho 1 hoặc nhiều bức ảnh mình tham gia.  3 bức ảnh nhận được lượt bình chọn cao nhất sẽ nhận “Giải bình chọn” của chương trình.</li>
        </ul>
        <strong>III- Quy cách ảnh dự thi:</strong>
        <ul>
            <li>Ảnh phải được chụp tại Việt Nam.</li>
            <li>Ảnh thể hiện được đặc điểm đặc trưng của vùng miền đã chọn.</li>
            <li>Ảnh chỉ được chỉnh sửa về màu sắc, không làm thay đổi bố cục và nội dung bức ảnh.</li>
        </ul>
        <p>
            Tất cả những ảnh không đáp ứng những quy cách trên sẽ bị BTC gỡ xuống không cần báo trước. 
        </p>
        <strong>V- Cơ cấu giải thưởng: </strong>
        <p>
            <strong>A- Cơ cấu giải thưởng:</strong>
        </p>
        <ol>
            <li>Giải bình chọn: 3 máy ảnh Canon IXUS 240 HS dành cho 3 người chơi có bức ảnh nhận được sự bình chọn cao nhất.</li>
            <li>Giải may mắn: 2 máy ảnh Canon IXUS 240 HS dành cho 2 người chơi may mắn khi “Rút thăm may mắn” vào cuối chương trình.</li>
        </ol>
        <p>
            Lưu ý: Người chơi chỉ nhận được tối đa 1 giải thưởng trong suốt cuộc thi.
        </p>
        <p>
            <strong>B- Thời gian và cách thức:</strong>
        </p>
        <ul>
            <li>Thời gian công bố kết quả: ngày 24/01/2013</li>
            <li>Thời gian nhận giải: từ ngày 25/01/2013 đến hết ngày 31/01/2013</li>
            <li>Cách thức nhận giải: 
                <ul>
                    <li>Đối với người trúng giải ở TPHCM: nhận giải trực tiếp tại văn phòng Canon Việt Nam, tầng 6, Saigon Centre, 65 Lê Lợi , Q1, TPHCM</li>
                    <li>Đối với người trúng giải ở các tỉnh khác: BTC sẽ liên hệ theo địa chỉ đã cung cấp để xác nhận. Giải thưởng sẽ được gửi qua đường bưu điện.</li>
                </ul>
            </li>
        </ul>
        <strong>VI- Quy định chung: </strong>
        <ul>
            <li>Các ảnh tham gia dự thi phải là tác phẩm chưa được đăng tải/công bố trên bất kỳ phương tiện thông tin đại chúng nào ,chưa từng tham gia cuộc thi nào, và không thuộc bất kì nguồn sở hữu nào.</li>
            <li>BTC sẽ không chịu trách nhiệm về việc tranh chấp tác quyền và tính pháp lý của tác phẩm. Người dự thi chịu mọi trách nhiệm liên quan đến vấn đề tác quyền của ảnh dự thi. </li>
            <li>BTC được sử dụng ảnh cho các mục đích quảng bá và truyền thông cho cuộc thi. Ảnh sử dụng trong các mục đích quảng cáo tiếp thị sẽ được BTC liên lạc và thương thảo bản quyền sử dụng ảnh cùng tác giả. </li>
            <li>Ảnh vi phạm thể lệ cuộc thi hay thuần phong mỹ tục Việt Nam sẽ bị loại bỏ mà không cần thông báo trước. </li>
            <li>Nếu có bằng chứng hiển nhiên người chơi gian lận và sao chép tác phẩm thì kết quả của người thắng cuộc sẽ bị tước bỏ.</li>
            <li>Nếu phát hiện tác phẩm trúng giải vi phạm thể lệ cuộc thi hoặc đến 24 giờ trước khi trao giải mà BTC vẫn chưa liên lạc được với người đoạt giải, BTC có quyền rút lại giải để trao cho người kế tiếp. </li>
            <li>BTC có quyền hủy kết quả giải thưởng trong trường hợp có tranh chấp và phần lỗi thuộc về người đoạt giải. </li>
            <li>Nếu có một vấn đề phát sinh trước trong hoặc sau cuộc thi, mà vấn đề này nằm ngoài quy định đang có, thì BTC sẽ giữ toàn quyền thảo luận để đưa ra quyết định xử lý vấn đề phát sinh đó.</li>
            <li>Quyết định của BTC là quyết định cuối cùng. Mọi tranh chấp, khiếu nại, thắc mắc về quyết định của BTC đều không có giá trị.</li>
        </ul>
    </div>
</asp:Content>
