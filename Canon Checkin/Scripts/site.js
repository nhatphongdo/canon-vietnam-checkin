﻿

var isTransparentUnderMouse = function (canvas, target, map, evnt) {
    var x = evnt.pageX - canvas.offsetLeft - canvas.parentNode.offsetLeft - map.x;
    var y = evnt.pageY - canvas.offsetTop - canvas.parentNode.offsetTop - map.y;
    var tempCanvas = document.createElement('canvas');
    tempCanvas.width = target.width;
    tempCanvas.height = target.height;
    tempCanvas.getContext('2d').drawImage(target, 0, 0);
    var imgdata = tempCanvas.getContext('2d').getImageData(x, y, 1, 1).data;
    if (imgdata[3] == 0) {
        return true;
    }
    return false;
};

var isTransparentUnderPosition = function (canvas, target, map, x, y) {
    var tempCanvas = document.createElement('canvas');
    tempCanvas.width = target.width;
    tempCanvas.height = target.height;
    tempCanvas.getContext('2d').drawImage(target, 0, 0);
    var imgdata = tempCanvas.getContext('2d').getImageData(x, y, 1, 1).data;
    if (imgdata[3] < 255) {
        return true;
    }
    return false;
};

var isInsideImage = function (canvas, map, evnt, target) {
    var x = evnt.pageX - canvas.offsetLeft - canvas.parentNode.offsetLeft;
    var y = evnt.pageY - canvas.offsetTop - canvas.parentNode.offsetTop;
    if (x >= target.x && x <= target.x + thumbSize && y >= target.y && y <= target.y + thumbSize) {
        return true;
    }
    return false;
};


var mapTitles = ["Trung du miền núi phía Bắc", "Bắc Trung Bộ", "Đồng bằng sông Hồng và duyên hải Đông Bắc", "Duyên hải Nam Trung Bộ", "Tây Nguyên", "Đông Nam Bộ", "Tây Nam Bộ"];
var mapNames = ["north", "northern-middle", "pink-river-land", "southern-middle", "western-highlands", "eastern-south", "western-south", "island"];
var maps = [
    { src: "Images/north.png", id: "north", x: 0, y: 0 },
    { src: "Images/north-hover.png", id: "north-hover" },
    { src: "Images/northern-middle.png", id: "northern-middle", x: 103, y: 170 },
    { src: "Images/northern-middle-hover.png", id: "northern-middle-hover" },
    { src: "Images/pink-river-land.png", id: "pink-river-land", x: 193, y: 108 },
    { src: "Images/pink-river-land-hover.png", id: "pink-river-land-hover" },
    { src: "Images/southern-middle.png", id: "southern-middle", x: 310, y: 440 },
    { src: "Images/southern-middle-hover.png", id: "southern-middle-hover" },
    { src: "Images/western-highlands.png", id: "western-highlands", x: 310, y: 500 },
    { src: "Images/western-highlands-hover.png", id: "western-highlands-hover" },
    { src: "Images/eastern-south.png", id: "eastern-south", x: 215, y: 685 },
    { src: "Images/eastern-south-hover.png", id: "eastern-south-hover" },
    { src: "Images/western-south.png", id: "western-south", x: 85, y: 765 },
    { src: "Images/western-south-hover.png", id: "western-south-hover" },
    { src: "Images/island.png", id: "island", x: 500, y: 380 }];

var preload;

function getMap(id) {
    for (var idx = 0; idx < maps.length; idx++) {
        if (maps[idx].id == id) {
            return maps[idx];
        }
    }

    return null;
}

function initializeMaps() {
    preload = new createjs.PreloadJS();
    preload.onProgress = handleProgress;
    preload.onComplete = handleComplete;
    preload.onFileLoad = handleFileLoad;
    preload.loadManifest(maps);
}

function handleProgress(event) {
    //bar.scaleX = (event * 100) * (loaderWidth / 100);
    //loaderStage.update();
    //if (bar.scaleX >= (100 * (loaderWidth / 100))) {
    //    loaderBar.visible = false;
    //    loaderStage.update();
    //}
}

function handleFileLoad(event) {
    //console.log(event);
}

function handleComplete(event) {
    $('.loading').hide();
    
    GetImages();

    drawMap();
    //drawImageMap();
    var canvas = document.getElementById("map-canvas");
    canvas.onmouseout = function (e) {
        drawMap();
    };

    canvas.onmousemove = function (e) {
        var selection;
        for (var idx = 0; idx < mapNames.length; idx++) {
            var image = preload.getResult(mapNames[idx]);
            var map = getMap(mapNames[idx]);
            if (!isTransparentUnderMouse(canvas, image.result, map, e)) {
                selection = mapNames[idx];
            }
        }

        drawMap(selection);

        //for (var idx = 0; idx < imagesOnMap.length; idx++) {
        //    if (isInsideImage(canvas, imagesOnMap[idx].map, e, imagesOnMap[idx])) {
        //        // Zoom in image
        //        var context = canvas.getContext("2d");
        //        var width = imagesOnMap[idx].image.width, height = imagesOnMap[idx].image.height;
        //        if (imagesOnMap[idx].image.width > 400) {
        //            height = imagesOnMap[idx].image.height * 400 / imagesOnMap[idx].image.width;
        //            width = 400;
        //        }
        //        if (imagesOnMap[idx].image.height > 300) {
        //            width = imagesOnMap[idx].image.width * 300 / imagesOnMap[idx].image.height;
        //            height = 300;
        //        }

        //        var x = imagesOnMap[idx].x, y = imagesOnMap[idx].y;
        //        x -= width / 2;
        //        if (x < 0) {
        //            x = 0;
        //        }

        //        var lineHeight;
        //        if (e.clientY < window.innerHeight / 2) {
        //            // Downside
        //            y += 100;
        //            lineHeight = 0;
        //        } else {
        //            y -= 100 + height;
        //            lineHeight = height;
        //        }

        //        context.drawImage(imagesOnMap[idx].image, x, y, width, height);

        //        context.beginPath();
        //        context.lineWidth = 2;
        //        context.strokeStyle = "#a0a0a0";
        //        context.shadowColor = "rgba(0, 0, 0, 0.5)";
        //        context.shadowOffsetX = 2;
        //        context.shadowOffsetY = 2;
        //        context.shadowBlur = 5;
        //        context.moveTo(imagesOnMap[idx].x, imagesOnMap[idx].y);
        //        context.lineTo(x, y + lineHeight);
        //        context.moveTo(imagesOnMap[idx].x + thumbSize, imagesOnMap[idx].y);
        //        context.lineTo(x + width, y + lineHeight);
        //        context.closePath();
        //        context.stroke();

        //        context.fillStyle = "rgba(0, 0, 0, 0.7)";
        //        context.fillRect(x, y + height, width, 30);
        //        context.textBaseline = "middle";
        //        context.fillStyle = "#ec1b23";
        //        context.font = "bold 14px Tahoma";
        //        context.fillText(imagesOnMap[idx].place, x + 20, y + height + 15);

        //        context.shadowColor = "rgba(0, 0, 0, 0)";
        //    }
        //}
    };
}

var thumbSize = 10;
function drawMap(selection) {
    var canvas = document.getElementById("map-canvas");
    var context = canvas.getContext('2d');
    context.clearRect(0, 0, canvas.width, canvas.height);
    for (var idx = 0; idx < mapNames.length; idx++) {
        var image;
        if (selection == mapNames[idx]) {
            image = preload.getResult(mapNames[idx] + "-hover");
            if (image == null) {
                image = preload.getResult(mapNames[idx]);
            }
        } else {
            image = preload.getResult(mapNames[idx]);
        }

        if (image == null) {
            continue;
        }

        var map = getMap(mapNames[idx]);
        context.drawImage(image.result, map.x, map.y, image.result.width, image.result.height);
    }

    if (window.location.href.toLowerCase().indexOf("default.aspx") < 0 &&
        window.location.href.toLowerCase().indexOf("upload.aspx") < 0 &&
        window.location.href.toLowerCase().indexOf(".aspx") >= 0) {
        // Draw text
        drawText(context, "Trung du miền núi phía Bắc", 260, 15, selection == "north");
        drawText(context, "Đồng bằng sông Hồng\nvà duyên hải Đông Bắc", 310, 170, selection == "pink-river-land");
        drawText(context, "Bắc Trung Bộ", 300, 340, selection == "northern-middle");
        drawText(context, "Quần đảo Hoàng Sa", 480, 370, selection == "");
        drawText(context, "Tây Nguyên", 250, 620, selection == "western-highlands");
        drawText(context, "Duyên hải Nam Trung Bộ", 470, 720, selection == "southern-middle");
        drawText(context, "Đông Nam Bộ", 170, 710, selection == "eastern-south");
        drawText(context, "Quần đảo Trường Sa", 480, 830, selection == "");
        drawText(context, "Tây Nam Bộ", 250, 890, selection == "western-south");
    }
    
    //draw images to map     
                      
    if (mapInfo == true) {
        drawImageMap();
    }
    else {
        for (var i = 0; i < imagesOnMap.length; i++) {
            if (imagesOnMap[i].image != undefined && imagesOnMap[i].x != undefined && imagesOnMap[i].y != undefined) {
                context.drawImage(imagesOnMap[i].image, imagesOnMap[i].x, imagesOnMap[i].y, thumbSize, thumbSize);
            }
        }
    }
}

function drawImageMap()
{
    var thumbsize = 10;
    var canvas = document.getElementById("map-canvas");
    var context = canvas.getContext('2d');
    var date = new Date();
    for (var i = 0; i < maps.length; i+=2)
    {
        var month = Math.round(date.getMonth()+1);
        var day = Math.round(date.getDate());
        var imageMapName = maps[i].id + day + month + date.getFullYear() + ".png";
        var image = new Image();
        image.src = "Images/" + imageMapName;
        context.drawImage(image, maps[i].x, maps[i].y, image.width, image.height);
    }
}


function drawText(context, text, x, y, isSelected) {
    if (isSelected) {
        context.font = "bold 12px Tahoma";
        context.fillStyle = "rgb(255, 0, 0)";
    } else {
        context.font = "normal 12px Tahoma";
        context.fillStyle = "rgb(0, 0, 0)";
    }

    var lines = text.split('\n');
    for (var i = 0; i < lines.length; i++) {
        context.fillText(lines[i], x, y + 18 * i);
    }
}

function setPage(css) {
    $('.page').addClass(css);
}

function setMenu(idx) {
    $('.menu li:eq(' + idx + ")").addClass('selected');
}

var W3CDOM = (document.createElement && document.getElementsByTagName);

function initFileUploads() {
    if (!W3CDOM) return;
    var fakeFileUpload = document.createElement('div');
    var fileInput = document.createElement('input');
    fileInput.setAttribute("readonly", "readonly");
    fileInput.setAttribute("placeholder", "Tập tin ảnh");
    fileInput.className = 'textbox upload-input';
    fakeFileUpload.appendChild(fileInput);
    var button = document.createElement('a');
    button.className = 'upload-button';
    button.textContent = "Chọn ảnh";
    fakeFileUpload.appendChild(button);
    var x = document.getElementsByTagName('input');
    for (var i = 0; i < x.length; i++) {
        if (x[i].type != 'file') continue;
        x[i].className = 'hidden';
        var clone = fakeFileUpload.cloneNode(true);
        x[i].parentNode.appendChild(clone);
        x[i].relatedElement = clone.getElementsByTagName('input')[0];
        x[i].relatedElement.onclick = function () {
            this.parentNode.parentNode.getElementsByTagName('input')[0].click();
        };
        clone.getElementsByTagName('a')[0].onclick = function () {
            this.parentNode.parentNode.getElementsByTagName('input')[0].click();
        };
        x[i].onchange = function () {
            this.relatedElement.value = this.value;
        };
        x[i].onselect = function () {
            this.relatedElement.select();
        };
    }
}

function showUserPopup(fullname, id, email, phone) {
    $('#user-popup .box');
    $('#user-popup .close').on("click", function () {
        $('#user-popup').fadeOut();
    });
    $('#user-popup').fadeIn();

    if (fullname != undefined) {
        $('#Fullname').val(fullname);
    }
    if (id != undefined) {
        $('#IDNumber').val(id);
    }
    if (email != undefined) {
        $('#Email').val(email);
    }
    if (phone != undefined) {
        $('#PhoneNumber').val(phone);
    }

    $('#user-popup div.error').html("");
}

function showNumberPopup(number) {
    $('#number-popup .box');
    $('#number-popup .close').on("click", function () {
        $('#number-popup').fadeOut();
    });
    $('#number-popup').fadeIn();

    if (number != undefined) {
        $('.lucky-box').text(number);
    }
}

function like(id) {
    $.ajax({
        url: "Like.aspx?id=" + id,
        success: function (result) {
            if (result == "1") {
                $('#message_' + id).html("<label style='color: #00afff;'>Bạn đã Like bức hình thành công</label>");
                $('#like_' + id).text(parseInt($('#like_' + id).text()) + 1);
            } else {
                $('#message_' + id).html("<label style='color: #f00;'>Bạn đã Like bức hình này rồi</label>");
            }
        }
    });
}

function showGallery() {
    $('#gallery-popup').fadeIn(function () {
        $('#gallery-popup ul li img').each(function () {
            $(this).cjObjectScaler({
                method: 'fit',
                fade: 550
            });
        });
    });
}

function showAlbum() {
    $('#album-popup').fadeIn(function () {
        $('.album li img').each(function () {
            $(this).cjObjectScaler({
                method: 'fit',
                fade: 550
            });
        });
    });
}

function showImage(id, image, like, fullname, place, city) {
    $('#album-popup .image-box img').attr("src", image);
    $('#album-popup .like-number').text(like);
    $('#album-popup .like-number').attr("id", "like_" + id);
    $('#album-popup .message').text("");
    $('#album-popup .message').attr("id", "message_" + id);
    $('#album-popup #fullname').text(fullname);
    $('#album-popup #place').text(place + ", " + city);
    $('#album-popup .like-button').attr("onclick", "like(" + id + ")");
    $('#album-popup .share-button').attr("onclick", "shareOnFacebook(" + id + ", '" + image + "')");

    $('#album-popup').fadeIn(function () {
        $('#album-popup .image-box img').cjObjectScaler({
            method: 'fit',
            fade: 550
        });
    });
}

var images;
function GetImages() {
    var FullmapInfo = new Array();
    if (typeof mapInfo != "undefined" && mapInfo != undefined && mapInfo != null) {
        FullmapInfo = mapInfo;
    }
    
    images = FullmapInfo;
    InitializeImages();
    
}

var imagesOnMap = new Array();
function InitializeImages() {
    var totalimages = images.length;
    var canvas = document.getElementById("map-canvas");
    imagesOnMap = new Array();
    for (var i = 0; i < mapNames.length; i++) {
        var map = getMap(mapNames[i]);
        var mapImage = preload.getResult(mapNames[i]).result;

        var count = imagesOnMap.length;
        for (var j = 0; j < totalimages; j++) {
            if (images[j].region == mapNames[i]) {
                imagesOnMap.push(images[j]);
            }
        }

        for (var y = 0; y < mapImage.height; y += thumbSize + 1) {
            for (var x = 0; x < mapImage.width; x += thumbSize + 1) {
                if (count >= imagesOnMap.length) {
                    break;
                }

                if (!isTransparentUnderPosition(canvas, mapImage, map, x, y) && !isTransparentUnderPosition(canvas, mapImage, map, x + thumbSize, y + thumbSize)) {
                    imagesOnMap[count].x = x + map.x;
                    imagesOnMap[count].y = y + map.y;
                    imagesOnMap[count].map = map;

                    ++count;
                }
            }

            if (count >= imagesOnMap.length) {
                break;
            }
        }
    }

    for (var i = 0; i < imagesOnMap.length; i++) {
        var image = new Image();
        image.setAttribute("index", i);
        image.onload = function () {
            var j = this.getAttribute("index");
            imagesOnMap[j].image = this;
        };
        image.src = imagesOnMap[i].src;
    }
}


function shareOnFacebook(id, image) {
    $('#facebookShare').attr("image_id", id);
    $('#facebookShare').attr("image_link", image);
    $('#facebookShare').click();
}