﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Canon_Checkin._Default" %>

<asp:Content runat="server" ID="FeaturedContent" ContentPlaceHolderID="HeadContent">
    <script type="text/javascript">
        $(document).ready(function () {
            setMenu(0);
            initializeMaps();
            setPage('home');
        })
    </script>
</asp:Content>
<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <canvas id="map-canvas" class="map" width="670" height="950"></canvas>
    <a class="likeBox"></a>
    <div class="intro-text">
        Cùng chia sẻ hình ảnh những nơi
        <br />
        bạn đã đi qua để chung tay vẽ nên
        <br />
        <span style="color: #ed1c24;">bản đồ ảnh lớn nhất Việt Nam.</span>
    </div>
    <a id="facebookLogin" class="button join">Tham gia ngay</a>
</asp:Content>
