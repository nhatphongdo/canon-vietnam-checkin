//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Canon_Checkin
{
    using System;
    using System.Collections.Generic;
    
    public partial class City
    {
        public City()
        {
            this.Photos = new HashSet<Photo>();
        }
    
        public int ID { get; set; }
        public string City1 { get; set; }
        public string Region { get; set; }
    
        public virtual ICollection<Photo> Photos { get; set; }
    }
}
