﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Canon_Checkin
{
    public partial class Like1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            using (var entities = new Canon_CheckinEntities())
            {
                var id = 0;
                if (int.TryParse(Request["id"], out id))
                {
                    var photo = entities.Photos.FirstOrDefault(p => p.ID == id);
                    if (photo != null)
                    {
                        var facebookID = Session["FacebookID"].ToString();
                        var like = entities.Likes.FirstOrDefault(l => l.FacebookID == facebookID && l.PhotoID == id);
                        if (like == null)
                        {
                            like = new Like()
                            {
                                CreatedDate = DateTime.Now,
                                FacebookID = facebookID,
                                IP = Utility.GetIPAddress(),
                                PhotoID = id
                            };
                            entities.Likes.Add(like);
                            entities.SaveChanges();

                            ++photo.Likes;
                            entities.SaveChanges();

                            Response.Write("1");
                        }
                        else
                        {
                            Response.Write("0");
                        }
                    }
                }
            }
        }
    }
}