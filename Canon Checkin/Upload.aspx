﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Upload.aspx.cs" Inherits="Canon_Checkin.Upload" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script src="Scripts/jquery.validate.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            setMenu(1);
            initializeMaps();
            setPage('upload');

            initFileUploads();

            var id = $('#City').val();
            var parts = id.split('_', 2);

            var titleIdx = mapNames.indexOf(parts[1]);
            $('#region').val(mapTitles[titleIdx]);

            $('#City').change(function () {
                id = $(this).val();
                parts = id.split('_', 2);

                titleIdx = mapNames.indexOf(parts[1]);
                $('#region').val(mapTitles[titleIdx]);
            });

            // Validate inputs
            $('form').validate({
                errorLabelContainer: $("form div.error"),
                onkeyup: false,
                onfocusout: false,
                invalidHandler: function (form, validator) {
                    $('form div.error label:not(.error)').remove();

                    $('#FileUpload').removeClass("error");
                    for (var i = 0; i < validator.errorList.length; i++) {
                        var error = validator.errorList[i];
                        if (error.element.id == "FileUpload") {
                            $(error.element).parent().children('div').children('input').addClass("error");
                        }
                    }

                    //$('form div.error').last('label:visible').css("padding-bottom", "20px");
                }
            });
            $('#City').rules("add", {
                required: true,
                messages: {
                    required: "Bạn chưa chọn tỉnh / thành nơi chụp bức ảnh này"
                }
            });

            $('#Place').rules("add", {
                required: true,
                messages: {
                    required: "Bạn chưa nhập địa điểm cụ thể nơi chụp bức ảnh này"
                }
            });

            $('#Fullname').rules("add", {
                required: true,
                messages: {
                    required: "Bạn chưa nhập Họ và Tên"
                }
            });
            $('#IDNumber').rules("add", {
                required: true,
                number: true,
                minlength: 9,
                maxlength: 9,
                messages: {
                    required: "Bạn chưa nhập Số CMND",
                    number: "Số CMND không hợp lệ",
                    minlength: "Số CMND không hợp lệ",
                    maxlength: "Số CMND không hợp lệ"
                }
            });
            $('#Email').rules("add", {
                required: true,
                email: true,
                messages: {
                    required: "Bạn chưa nhập Địa chỉ email",
                    email: "Địa chỉ email không hợp lệ"
                }
            });
            $('#PhoneNumber').rules("add", {
                required: true,
                number: true,
                minlength: 6,
                maxlength: 15,
                messages: {
                    required: "Bạn chưa nhập Số điện thoại",
                    number: "Số điện thoại không hợp lệ",
                    minlength: "Số điện thoại không hợp lệ",
                    maxlength: "Số điện thoại không hợp lệ"
                }
            });
            $('#Agreement').rules("add", {
                required: true,
                minlength: 1,
                maxlength: 1,
                messages: {
                    required: "Bạn phải đồng ý với Điều khoản của chương trình",
                    minlength: "Bạn phải đồng ý với Điều khoản của chương trình",
                    maxlength: "Bạn phải đồng ý với Điều khoản của chương trình"
                }
            });
            $('#FileUpload').rules("add", {
                required: true,
                accept: "png|jpe?g",
                messages: {
                    required: "Bạn chưa chọn ảnh",
                    accept: "Ảnh phải có định dạng JPEG hoặc PNG và dung lượng dưới 1MB"
                }
            });
        })
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <canvas id="map-canvas" class="map" width="670" height="950"></canvas>
    <asp:Literal runat="server" ID="ShareLink"></asp:Literal>
    <div class="upload-form">
        Cùng chia sẻ hình ảnh những nơi bạn đã đi qua để chung tay
        <br />
        vẽ nên bản đồ ảnh lớn nhất Việt Nam.
        <br />
        <br />
        <br />
        <div class="error">
            <asp:Literal runat="server" ID="ErrorMessage"></asp:Literal>
        </div>
        <strong>Bước 1:</strong> Chọn bức ảnh bạn chụp với <strong>Canon IXUS</strong>
        <div>
            <asp:FileUpload runat="server" ID="FileUpload" />
        </div>
        <br />
        <br />
        <br />
        <div class="clear" style="color: #505050; font-size: 11px; margin-top: -10px; font-style: italic;">
            (Ảnh dự thi không được quá 1MB)
        </div>
        <br />
        <br />
        <strong>Bước 2:</strong> Chọn tỉnh / thành nơi bạn chụp bức ảnh này
        <br />
        <asp:DropDownList runat="server" ID="City" CssClass="combo-box" ClientIDMode="Static" DataValueField="ID" DataTextField="City" />
        <br />
        <br />
        Thuộc vùng:
        <br />
        <input id="region" class="textbox" readonly="readonly" />
        <br />
        <br />
        <br />
        <strong>Bước 3:</strong> Nhập địa điểm cụ thể nơi bạn chụp ảnh
        <br />
        <asp:TextBox runat="server" ID="Place" CssClass="textbox" ClientIDMode="Static" placeholder="vd: nhà thờ Đức Bà, lăng Khải Định, ..."></asp:TextBox>
        <br />
        <br />
        <asp:Button runat="server" ID="UploadPhoto" Text="Hoàn tất" CssClass="button float-right" OnClick="UploadPhoto_Click" />
    </div>
    <div id="user-popup" class="shadow">
        <div class="box">
            <a class="close">X</a>
            <div class="clear"></div>
            <div>Vui lòng điền thông tin của bạn để <strong>Canon</strong> liên hệ khi <strong>trao giải</strong></div>
            <table>
                <tr>
                    <td colspan="2">
                        <div class="error">
                            <asp:Literal runat="server" ID="ErrorMessage2"></asp:Literal>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for="Fullname">Họ và tên:</label></td>
                    <td>
                        <asp:TextBox runat="server" ID="Fullname" ClientIDMode="Static" CssClass="textbox"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for="IDNumber">CMND:</label>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="IDNumber" ClientIDMode="Static" CssClass="textbox"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for="Email">Email:</label>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="Email" ClientIDMode="Static" CssClass="textbox"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for="PhoneNumber">Điện thoại:</label>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="PhoneNumber" ClientIDMode="Static" CssClass="textbox"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <asp:CheckBox runat="server" ID="Agreement" ClientIDMode="Static" CssClass="checkbox" />
                        <label for="Agreement">Tôi đã đọc và đồng ý những <a href="Term.aspx">điều kiện và điều khoản của chương trình.</a></label>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <asp:Button runat="server" ID="UpdateUser" CssClass="button float-right" Text="Tiếp Theo" OnClick="UpdateUser_Click" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div id="number-popup" class="shadow">
        <div class="box">
            <a class="close">X</a>
            <div class="clear"></div>
            Cảm ơn bạn đã đăng ảnh góp phần tạo nên bản đồ Việt Nam cùng IXUS
            <br />
            <br />
            <strong>Mã số may mắn của bạn</strong>
            <br />
            <div class="lucky-box"></div>
            <br />
            Cùng xem những hình ảnh đẹp của Việt Nam trên bản đồ của bạn
            <br />
            <a class="button" href="MyMap.aspx">Xem Bản Đồ</a>
        </div>
    </div>
</asp:Content>
