﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Canon_Checkin
{
    public partial class Upload : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                // Load cities
                using (var entities = new Canon_CheckinEntities())
                {
                    var cities = entities.Cities.ToList().Select(c => new
                                                                 {
                                                                     ID = c.ID.ToString() + "_" + c.Region,
                                                                     City = c.City1
                                                                 }).OrderBy(c => c.City).ToList();

                    cities.Insert(0, new { ID = string.Empty, City = "Chọn tỉnh / thành" });

                    City.DataSource = cities;
                    City.DataBind();
                }
            }
        }

        protected void UploadPhoto_Click(object sender, EventArgs e)
        {
            if (!FileUpload.HasFile)
            {
                ErrorMessage.Text = "<label class='error'>Bạn phải chọn tập tin ảnh để tải lên</label>";
                return;
            }

            if (!FileUpload.FileName.ToLower().EndsWith(".jpg") && !FileUpload.FileName.ToLower().EndsWith(".jpeg") && !FileUpload.FileName.ToLower().EndsWith(".png"))
            {
                ErrorMessage.Text = "<label class='error'>Tập tin ảnh phải có định dạng JPEG hoặc PNG</label>";
                return;
            }

            if (FileUpload.PostedFile.ContentLength > 1024768)
            {
                ErrorMessage.Text = "<label class='error'>Tập tin ảnh phải có dung lượng không quá 1 MB</label>";
                return;
            }

            if (string.IsNullOrEmpty(City.SelectedValue))
            {
                ErrorMessage.Text = "<label class='error'>Bạn phải chọn tỉnh / thành nơi chụp bức ảnh này</label>";
                return;
            }

            var cityValues = City.SelectedValue.Split(new char[]
                                                         {
                                                             '_'
                                                         }, StringSplitOptions.RemoveEmptyEntries);
            var cityID = 0;
            if (cityValues.Length > 0)
            {
                int.TryParse(cityValues[0], out cityID);
            }

            if (cityID <= 0)
            {
                ErrorMessage.Text = "<label class='error'>Bạn phải chọn tỉnh / thành nơi chụp bức ảnh này</label>";
                return;
            }

            if (string.IsNullOrEmpty(Place.Text))
            {
                ErrorMessage.Text = "<label class='error'>Bạn phải nhập địa điểm cụ thể nơi chụp bức ảnh này</label>";
                return;
            }

            using (var entities = new Canon_CheckinEntities())
            {
                var facebookID = Session["FacebookID"].ToString();
                var user = entities.Users.FirstOrDefault(u => u.FacebookID == facebookID);
                if (user == null)
                {
                    Response.Redirect("Default.aspx");
                    return;
                }

                var fileName = string.Format("Upload/{0}_{1}.jpg", user.ID, DateTime.Now.Ticks);
                try
                {
                    var bitmap = Bitmap.FromStream(FileUpload.PostedFile.InputStream);

                    var propertyItems = bitmap.PropertyItems;
                    foreach (var property in propertyItems)
                    {
                        if (property.Id == 274)
                        {
                            // Orientation
                            switch (property.Value[0])
                            {
                                case 1:
                                    // Keep as original
                                    break;
                                case 2:
                                    // Flip X
                                    bitmap.RotateFlip(RotateFlipType.RotateNoneFlipX);
                                    break;
                                case 3:
                                    // Rotate right 90 degree
                                    bitmap.RotateFlip(RotateFlipType.Rotate90FlipNone);
                                    break;
                                case 4:
                                    // Flip Y
                                    bitmap.RotateFlip(RotateFlipType.RotateNoneFlipY);
                                    break;
                                case 5:
                                    // Rotate right 90 degree and flip X
                                    bitmap.RotateFlip(RotateFlipType.Rotate90FlipX);
                                    break;
                                case 6:
                                    // Rotate right 90 degree
                                    bitmap.RotateFlip(RotateFlipType.Rotate90FlipNone);
                                    break;
                                case 7:
                                    // Rotate left 90 degree and flip X
                                    bitmap.RotateFlip(RotateFlipType.Rotate270FlipX);
                                    break;
                                case 8:
                                    // Rotate left 90 degree
                                    bitmap.RotateFlip(RotateFlipType.Rotate270FlipNone);
                                    break;
                            }
                        }
                    }

                    var width = bitmap.Width;
                    var height = bitmap.Height;

                    if (width > 1000)
                    {
                        height = height * 1000 / width;
                        width = 1000;
                    }

                    if (height > 1000)
                    {
                        width = width * 1000 / height;
                        height = 1000;
                    }

                    var resizedBitmap = new Bitmap(bitmap, width, height);
                    resizedBitmap.Save(Server.MapPath(fileName), System.Drawing.Imaging.ImageFormat.Jpeg);
                }
                catch (Exception exc)
                {
                    ErrorMessage.Text = "<label class='error'>Xảy ra lỗi trong quá trình tải ảnh lên. Bạn hãy tải lại trang và thử lại.</label>";
                    return;
                }

                var photo = new Photo()
                {
                    IP = Utility.GetIPAddress(),
                    Likes = 0,
                    PhotoPath = fileName,
                    UploadedDate = DateTime.Now,
                    UserID = user.ID,
                    City = cityID,
                    Place = Place.Text,
                    LuckyCode = GetLuckyCode(6),
                    IsPublished = true
                };
                entities.Photos.Add(photo);
                entities.SaveChanges();

                if (string.IsNullOrEmpty(user.FullName) || string.IsNullOrEmpty(user.IDNumber) || string.IsNullOrEmpty(user.Email) || string.IsNullOrEmpty(user.PhoneNumber))
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "showUserPopupFunc",
                        string.Format("<script type='text/javascript'>showUserPopup('{0}', '{1}', '{2}', '{3}');</script>", user.FullName, user.IDNumber, user.Email, user.PhoneNumber),
                        false);
                }
                else
                {
                    //ShareLink.Text = string.Format("<a id='facebookShare2' image_id='{0}' image_link='{1}' style='display: none'>chia sẻ lên facebook</a>", photo.ID, photo.PhotoPath);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "showNumberPopupFunc",
                                                        string.Format("<script type='text/javascript'>showNumberPopup('{0}'); setTimeout(\"shareOnFacebook({1}, '{2}');\", 2000);</script>", photo.LuckyCode, photo.ID, photo.PhotoPath),
                                                        false);
                }
            }
        }

        protected string GetLuckyCode(int length)
        {
            const string codes = "0123456789";
            var random = new Random((int)DateTime.Now.Ticks);
            var code = string.Empty;

            using (var entities = new Canon_CheckinEntities())
            {
                do
                {
                    code = string.Empty;
                    for (var i = 0; i < length; i++)
                    {
                        code += codes[random.Next(codes.Length)];
                    }
                }
                while (entities.Photos.Any(p => p.LuckyCode == code));
            }

            return code;
        }

        protected void UpdateUser_Click(object sender, EventArgs e)
        {
            using (var entities = new Canon_CheckinEntities())
            {
                var facebookID = Session["FacebookID"].ToString();
                var user = entities.Users.FirstOrDefault(u => u.FacebookID == facebookID);
                if (user == null)
                {
                    Response.Redirect("Default.aspx");
                    return;
                }

                // Validate
                if (string.IsNullOrEmpty(Fullname.Text))
                {
                    ErrorMessage2.Text = "<label class='error'>Bạn chưa nhập Họ và Tên</label>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "showUserPopupFunc", "<script type='text/javascript'>showUserPopup();</script>", false);
                    return;
                }

                if (string.IsNullOrEmpty(IDNumber.Text))
                {
                    ErrorMessage2.Text = "<label class='error'>Bạn chưa nhập Số CMND</label>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "showUserPopupFunc", "<script type='text/javascript'>showUserPopup();</script>", false);
                    return;
                }

                var regexIDNumber = new System.Text.RegularExpressions.Regex(@"^[0-9]+$");
                if (!regexIDNumber.IsMatch(IDNumber.Text) || IDNumber.Text.Length < 9 || IDNumber.Text.Length > 9)
                {
                    ErrorMessage2.Text = "<label class='error'>Số CMND không hợp lệ</label>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "showUserPopupFunc", "<script type='text/javascript'>showUserPopup();</script>", false);
                    return;
                }

                var oldUser = entities.Users.FirstOrDefault(u => u.IDNumber == IDNumber.Text);
                if (oldUser != null && oldUser.ID != user.ID)
                {
                    ErrorMessage2.Text = "<label class='error'>Số CMND này đã được sử dụng</label>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "showUserPopupFunc", "<script type='text/javascript'>showUserPopup();</script>", false);
                    return;
                }

                if (string.IsNullOrEmpty(Email.Text))
                {
                    ErrorMessage2.Text = "<label class='error'>Bạn chưa nhập Địa chỉ Email</label>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "showUserPopupFunc", "<script type='text/javascript'>showUserPopup();</script>", false);
                    return;
                }

                var regexEmail = new System.Text.RegularExpressions.Regex(@"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?");
                if (!regexEmail.IsMatch(Email.Text))
                {
                    ErrorMessage2.Text = "<label class='error'>Địa chỉ Email không hợp lệ</label>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "showUserPopupFunc", "<script type='text/javascript'>showUserPopup();</script>", false);
                    return;
                }

                oldUser = entities.Users.FirstOrDefault(u => u.Email.ToLower() == Email.Text.ToLower());
                if (oldUser != null && oldUser.ID != user.ID)
                {
                    ErrorMessage2.Text = "<label class='error'>Địa chỉ Email này đã được sử dụng</label>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "showUserPopupFunc", "<script type='text/javascript'>showUserPopup();</script>", false);
                    return;
                }

                if (string.IsNullOrEmpty(PhoneNumber.Text))
                {
                    ErrorMessage2.Text = "<label class='error'>Bạn chưa nhập Số điện thoại</label>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "showUserPopupFunc", "<script type='text/javascript'>showUserPopup();</script>", false);
                    return;
                }

                var regexPhoneNumber = new System.Text.RegularExpressions.Regex(@"^[0-9]+$");
                if (!regexPhoneNumber.IsMatch(PhoneNumber.Text) || PhoneNumber.Text.Length < 6 || PhoneNumber.Text.Length > 15)
                {
                    ErrorMessage2.Text = "<label class='error'>Số điện thoại không hợp lệ</label>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "showUserPopupFunc", "<script type='text/javascript'>showUserPopup();</script>", false);
                    return;
                }

                if (!Agreement.Checked)
                {
                    ErrorMessage2.Text = "<label class='error'>Bạn phải Đồng ý với điều khoản của chương trình</label>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "showUserPopupFunc", "<script type='text/javascript'>showUserPopup();</script>", false);
                    return;
                }

                user.FullName = Fullname.Text;
                user.IDNumber = IDNumber.Text;
                user.Email = Email.Text;
                user.PhoneNumber = PhoneNumber.Text;
                entities.SaveChanges();

                var photo =
                    entities.Photos.Where(p => p.UserID == user.ID).OrderByDescending(p => p.UploadedDate).
                        FirstOrDefault();

                if (photo != null)
                {
                    ShareLink.Text = string.Format("<a class='share' image_id={0} image_link={1} style='display: none'>chia sẻ lên facebook</a>", photo.ID, photo.PhotoPath);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "showNumberPopupFunc",
                                                        string.Format("<script type='text/javascript'>showNumberPopup('{0}');</script>", photo.LuckyCode),
                                                        false);
                }
            }
        }
    }
}